import unittest
from canonical_phone.phone import canonical_number


class TestCanonicalPhone(unittest.TestCase):
    def setUp(self):
        self.combinations = [
            {
                "input": "62-8734878374",
                "output": "62-8734878374",
            },
            {
                "input": "628734878374",
                "output": "62-8734878374",
            },
            {
                "input": "8734878374",
                "output": "62-8734878374",
            },
            {
                "input": "84-8734878374",
                "output": "84-8734878374",
            },
            {
                "input": "91-8734878374",
                "output": "91-8734878374",
            },
            {
                "input": "848734878374",
                "output": "62-848734878374",
            },
            {
                "input": "848734878374",
                "output": "84-8734878374",
                "has_country_code": True,
            },
            {
                "input": "848734878374",
                "output": "62-848734878374",
                "has_country_code": False,
            },
            {
                "input": "91848734878374",
                "output": "91-848734878374",
                "has_country_code": True,
            },
            {
                "input": "848700694057",
                "output": "84-8700694057",
                "has_country_code": True,
            },
        ]

    def test_valid_indonesian_phone(self):
        for number in self.combinations:
            self.assertEqual(canonical_number(phone_no=number['input'],
                                              has_country_code=number.get('has_country_code', False)), number['output'])


if __name__ == '__main__':
    unittest.main()
